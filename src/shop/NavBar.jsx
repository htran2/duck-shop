import { Link, NavLink } from "react-router-dom"
import { changeCurrency, currency } from './redux/shoppingSlice'
import { useSelector, useDispatch } from 'react-redux'

const NavBar = () => {
  const dispatch = useDispatch()
  const setCurrency = (newCurrency) => {
    dispatch(changeCurrency(newCurrency))
  }
  return (
    <div className="nav-bar">
      <NavLink to="/">Home</NavLink>
      <NavLink to="/cart">Cart</NavLink>
      <NavLink to="/orders">Orders</NavLink>
      <span>
        <input type="radio" value="CAD" name="currency"
          onChange={() => setCurrency('CAD')} checked={useSelector(currency) === 'CAD'} 
        />
        {`CAD `}
        <input type="radio" value="EUR" name="currency"
          onChange={() => setCurrency('EUR')} checked={useSelector(currency) === 'EUR'}
        />
        {`EUR `}
      </span>
    </div>
  )
}
export default NavBar
