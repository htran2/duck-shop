const products = [
  { id: '1', name: 'Milk', price: 10, },
  { id: '2', name: 'Cheese', price: 20, },
  { id: '3', name: 'Salami', price: 30, },
]

export function getProductById(id) {
  return products.find(p => id === p.id)
}

export function getAllProducts() {
  return products;
}

