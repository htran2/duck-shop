import { Link } from "react-router-dom"
import NavBar from './NavBar'
import { orders } from './redux/orderingSlice'
import { useSelector, useDispatch } from 'react-redux'
import { getProductById } from './services'

export default function Orders() {
  let allOrders = <h3>No orders</h3>;
  if (useSelector(orders).length > 0) {
    allOrders = <ul>{useSelector(orders).map(order => <OrderListItem {...order} key={order.datetime} />)}</ul>
  }
  return (
    <>
      <NavBar />
      <h1>Orders</h1>
      {allOrders}
    </>
  )
}

const OrderListItem = ({datetime, status, products}) => {
  const allProductItems = (
    <ul>
      {products.map(p => {
        const product = getProductById(p.productId)
        return (
          <li key={product.id}>
            <Link to={`/products/${product.id}`}>{product.name}</Link>
            {` ${p.quantity} x ${p.price} ${p.currency}`}
          </li>
        )
      })}
    </ul>
  )
  const dt = new Date(Date.parse(datetime))
  return (
    <li key={datetime}>
      <h3>{`${dt.toDateString()} ${dt.toLocaleTimeString()} - ${status}`}</h3>
      {allProductItems}
      {status === 'ORDERED' && <button onClick={() => dispatch(removeFromCart(productId))}>
        Cancel this order
      </button>}
      <hr />
    </li>
  )
}
