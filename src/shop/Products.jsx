import { Link, NavLink } from "react-router-dom"
import NavBar from './NavBar'
import { getAllProducts, getProductById } from './services'
import { currency, exchangeRate, addToCart } from './redux/shoppingSlice'
import { useSelector, useDispatch } from 'react-redux'

const Products = () => {
  const all = getAllProducts().map(p => <ProductListItem productId={p.id} key={p.id} />)
  return (
    <>
      <NavBar />
      <h1>Products</h1>
      <ul>{all}</ul>
    </>
  )
}

const ProductListItem = ({productId}) => {
  const product = getProductById(productId)
  const dispatch = useDispatch()
  return (
    <li>
      <Link to={`/products/${productId}`}>{product.name}</Link>
      <span>{` ${product.price * useSelector(exchangeRate)} ${useSelector(currency)}`}</span>
      <button onClick={() => dispatch(addToCart({ id: productId, quantity: 1 }))}>
        Add to cart
      </button>
    </li>
  )
}

export default Products