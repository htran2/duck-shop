import { Link, NavLink } from "react-router-dom"
import { changeCurrency, addToCart, all } from './shoppingSlice'
import { useSelector, useDispatch } from 'react-redux'

// all state data in redux store, and how to change them

const StoreDump = () => {
  const dispatch = useDispatch()
  return (
    <>
      <h1>Store all state data</h1>
      <h3>{JSON.stringify(useSelector(all))}</h3>
      <hr />
      <h1>Store state playground</h1>
      <button onClick={() => dispatch(changeCurrency('EUR'))}>Change to EUR</button>
      <button onClick={() => dispatch(changeCurrency('CAD'))}>Change to CAD</button>
      <div>
        <button onClick={() => dispatch(addToCart({ id: '1', quantity: 1 }))}>
          Add to cart
        </button>
        (product id=1, qty=1)
        <br />
        <button onClick={() => dispatch(addToCart({ id: '2', quantity: 3 }))}>
          Add to cart
        </button>
        (product id=2, qty=3)
      </div>    
    </>
  )
}

export default StoreDump