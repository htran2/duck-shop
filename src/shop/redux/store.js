import { configureStore } from '@reduxjs/toolkit'
import shoppingReducer from './shoppingSlice'
import orderingReducer from './orderingSlice'

export const store = configureStore({
  reducer: {
    ordering: orderingReducer,
    shopping: shoppingReducer,
  }
})




// more slices:
// user account
// payments
// refunds
