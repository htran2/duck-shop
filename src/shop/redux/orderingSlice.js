import { createSlice } from '@reduxjs/toolkit'
import { getProductById } from '../services'

export const orderingSlice = createSlice({
  name: 'ordering',
  initialState: {
    orders: []
  },
  reducers: {
    addToOrders: (state, action) => {
      const products = []
      const order = action.payload.cartContents.forEach(item => {
        const product = getProductById(item.id)
        products.push({
          productId: product.id,
          quantity: item.quantity,
          price: product.price,
          currency: action.payload.currency,
        })
      })
      state.orders.push({
        datetime: new Date().toISOString(),
        status: 'ORDERED',
        products,
      })
    },
    changeStatus: (state, action) => {
      
    },
    cancelOrder: (state, action) => {
    },
  },
})

export const { addToOrders, cancelOrder, loadOrders } = orderingSlice.actions
export const orders = appState => appState.ordering.orders
export default orderingSlice.reducer


