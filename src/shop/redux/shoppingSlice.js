import { createSlice } from '@reduxjs/toolkit'

export const shoppingSlice = createSlice({
  name: 'shopping',
  initialState: {
    currency: 'CAD',
    cart: [], // array of products
  },
  reducers: {
    changeCurrency: (state, action) => {
      state.currency = action.payload
    },
    addToCart: (state, action) => {
      const itemToAdd = action.payload
      const index = state.cart.findIndex(item => itemToAdd.id === item.id)
      if (index >= 0) { // added previously
        state.cart[index].quantity += itemToAdd.quantity
      } else { // newly added
        state.cart.push(action.payload) 
      }
    },
    removeFromCart: (state, action) => {
      const idToRemove = action.payload
      const index = state.cart.findIndex(item => idToRemove === item.id)
      if (index >= 0) {
        state.cart.splice(index, 1)
      }
    },
    clearCart: (state, action) => {
      state.cart = []
    },
  },
})

export const { changeCurrency, addToCart, removeFromCart, clearCart } = shoppingSlice.actions
export const currency = appState => appState.shopping.currency
export const exchangeRate = appState => appState.shopping.currency === 'EUR' ? 0.7 : 1.0
export const cart = appState => appState.shopping.cart
export const all = appState => appState.shopping
export default shoppingSlice.reducer
