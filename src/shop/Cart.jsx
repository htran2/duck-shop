import { Link } from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux'
import { cart, currency, exchangeRate, removeFromCart, clearCart } from './redux/shoppingSlice'
import { addToOrders } from './redux/orderingSlice'
import NavBar from './NavBar'
import { getProductById } from './services'


export default function Cart() {
  const dispatch = useDispatch()
  const curr = useSelector(currency)
  const cartContents = useSelector(cart)
  let cartItems = <h3>Cart is empty</h3>
  if (cartContents.length > 0) {
    cartItems = <ul>{cartContents.map(p => <CartListItem productId={p.id} key={p.id} quantity={p.quantity} />)}</ul>
  }
  return (
    <>
      <NavBar />
      <h1>Cart</h1>
      {cartItems}
      {cartContents.length > 0 && <>
        <button onClick={() => dispatch(clearCart())}>Clear cart</button>
        <button onClick={() => {
          dispatch(clearCart())
          dispatch(addToOrders({cartContents, currency: curr}))
        }}>Order</button>
      </>}
    </>
  )
}

const CartListItem = ({productId, quantity}) => {
  const product = getProductById(productId)
  const rate = useSelector(exchangeRate)
  const curr = useSelector(currency)
  const dispatch = useDispatch()
  return (
    <li>
      <Link to={`/products/${productId}`}>{product.name}</Link>
      <span>{quantity}</span>
      <span>{` x ${product.price * rate} ${curr}`}</span>
      <span>{` = ${product.price * rate * quantity} ${curr}`}</span>
      <button onClick={() => dispatch(removeFromCart(productId))}>
        Remove
      </button>
    </li>
  )
}