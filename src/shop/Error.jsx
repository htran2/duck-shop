import { useRouteError } from "react-router-dom";

export default function RouteError() {
  const error = useRouteError();
  console.error(error);

  return (
    <>
      <h1>Some error!</h1>
      <p>
        <i>{error.statusText || error.message || 'URL not found.'}</i>
      </p>
    </>
  );
}