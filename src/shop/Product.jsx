import { Link, NavLink, useParams } from "react-router-dom"
import { getProductById } from './services'
import { currency, exchangeRate, addToCart } from './redux/shoppingSlice'
import { useSelector, useDispatch } from 'react-redux'
import NavBar from './NavBar'

export default function Product(props) {
  const { productId } = useParams()
  const product = getProductById(productId)
  const dispatch = useDispatch()
  return (
    <>
      <NavBar />
      <h1>Product details</h1>
      <h3>{`ID: ${product.id}`}</h3>
      <h3>{`Name: ${product.name}`}</h3>
      <h3>{`Price=${product.price * useSelector(exchangeRate)} ${useSelector(currency)}`}</h3>
      <button onClick={() => dispatch(addToCart({ id: product.id, quantity: 1 }))}>
        Add to cart
      </button>
    </>
  )
}

