import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import { store } from './shop/redux/store'
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Products from './shop/Products'
import Product from './shop/Product'
import Cart from './shop/Cart'
import Orders from './shop/Orders'
import Error from './shop/Error'
import StoreDump from'./shop/redux/store-dump'
import './shop/shop.scss'

const router = createBrowserRouter([
  {
    path: "/",
    element: <Products />,
    errorElement: <Error />,
  },
  {
    path: "/products/:productId",
    element: <Product />,
  },
  {
    path: "/cart",
    element: <Cart />,
  },
  {
    path: "/orders",
    element: <Orders />,
  },
  {
    path: "/store-dump",
    element: <StoreDump />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
