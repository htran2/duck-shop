# A little demo of react redux


## About
A shopping app, with products in CAD or EUR, and management of currency, shopping cart, ordering. These managements are done with redux.

React setup is with "vitejs" (less complicated than webpack or CRA)
Routing is the usual `react-router-dom`

## Set up

```
git clone https://gitlab.com/htran2/duck-shop.git

cd duck-shop

npm install

npm start

```

Visit `http://127.0.0.1:5173/`


## Status
- Products are hard coded
- Cart management is ok, seems to work fine
- Ordering needs more functionalities
- Lots of things you can try


## Reading code
- Start from main.jsx
- business components are in /src/shop/
- redux stuff in src/shop/redux
- Go to `http://127.0.0.1:5173/store-dump` to see examples of getters/setters used in `store-dump.jsx`

